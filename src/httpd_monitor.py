#!/path/to/python

import difflib
import logging
import shutil
import subprocess
import sys
from pathlib import Path

import yaml

logging.basicConfig(format='%(levelname)s - %(message)s', level=logging.INFO)


class HttpdMonitor:

    def __init__(self, yaml_file):
        self.yaml_file = yaml_file
        self.config = self.load_config()
        self.file_to_watch = self.config['httpd-conf']
        self.backup = f'{self.file_to_watch}.bak'
        self.syntax_check_cmd = self.config['syntax-check-cmd'].split()
        self.reload_cmd = self.config['reload-cmd'].split()

    def load_config(self):
        """Open YAML file to load the config.
        :return: Config object
        """
        with open(self.yaml_file, 'r') as stream:
            return yaml.safe_load(stream)

    def make_backup(self):
        """Copy and rename file to make a backup."""
        try:
            shutil.copy2(self.file_to_watch, self.backup)
        except IOError as e:
            logging.exception('Making backup failed: %s', e)

    def log_diff(self):
        """Log file content differences."""
        try:
            diffs = self.find_diff()
            logging.info('Differences found: \n%s', ''.join(diffs))
        except IOError as e:
            logging.exception('Finding diff failed: %s', e)

    def find_diff(self):
        """Diff two files.
        :return: A delta (a generator generating the delta lines) in unified diff format
        """
        with open(self.file_to_watch, 'r') as f, open(self.backup, 'r') as bak:
            diffs = difflib.unified_diff(f.readlines(), bak.readlines(), fromfile=self.file_to_watch,
                                         tofile=self.backup)
            return diffs

    def reload_httpd(self):
        """Handle httpd reload."""
        try:
            self.call_commands()
            logging.info('Reload successful!')
        except subprocess.CalledProcessError as e:
            logging.exception('Reloading failed: %s', e)

    def call_commands(self):
        """Call shell commands."""
        subprocess.run(self.syntax_check_cmd, check=True)
        subprocess.run(self.reload_cmd, check=True)

    def run(self):
        if not Path(self.backup).is_file():
            self.make_backup()
            return
        self.log_diff()
        self.make_backup()
        self.reload_httpd()


if __name__ == '__main__':
    httpd_monitor = HttpdMonitor(sys.argv[1])
    httpd_monitor.run()
