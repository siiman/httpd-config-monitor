import datetime
import filecmp
import subprocess
import unittest
from unittest.mock import patch, mock_open

from src.httpd_monitor import HttpdMonitor


class HttpdMonitorTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        test_yaml = './resources/test_config.yaml'
        cls.hm = HttpdMonitor(test_yaml)

    # load_config tests
    def test_loading_config(self):
        config = self.hm.load_config()
        self.assertEqual(config['reload-cmd'], 'true')

    # make_backup() tests
    def test_creating_backup(self):
        with open(self.hm.file_to_watch, 'w') as f:
            f.write('Testing on %s' % datetime.datetime.now())
        self.hm.make_backup()
        self.assertTrue(filecmp.cmp(self.hm.file_to_watch, self.hm.backup))

    def test_making_backup_when_file_not_exists(self):
        with patch('shutil.copy2') as mo:
            mo.side_effect = FileNotFoundError()
            with self.assertLogs() as log:
                self.hm.make_backup()
            self.assertIn('Making backup failed', log.output[0])

    # log_diff() tests
    def test_is_diff_logged(self):
        with self.assertLogs() as log:
            self.hm.log_diff()
        self.assertIn('Differences found', log.output[0])

    def test_logging_diff_when_file_not_found(self):
        with patch('builtins.open') as mo:
            mo.side_effect = FileNotFoundError()
            with self.assertLogs() as log:
                self.hm.log_diff()
            self.assertIn('Finding diff failed', log.output[0])

    # find_diff() tests
    def test_find_diff_w_different_content_files(self):
        with patch('builtins.open', mock_open(read_data='test')) as mo:
            mo.side_effect = [mo.return_value, mock_open(read_data='not a test').return_value]
            diff = self.hm.find_diff()
            self.assertEqual('+not a test', list(diff)[-1])

    def test_find_diff_w_same_content_files(self):
        with patch('builtins.open', mock_open(read_data='test')) as mo:
            mo.side_effect = [mo.return_value, mock_open(read_data='test').return_value]
            diff = self.hm.find_diff()
            self.assertFalse(list(diff))

    # reload_httpd() tests
    def test_reloading_httpd_with_return_code_1(self):
        with patch('subprocess.run') as mo:
            mo.side_effect = subprocess.CalledProcessError(1, 'run')
            with self.assertLogs() as log:
                self.hm.reload_httpd()
            self.assertIn('Reloading failed', log.output[0])

    def test_reloading_httpd_with_return_code_0(self):
        with self.assertLogs() as log:
            self.hm.reload_httpd()
        self.assertIn('Reload successful', log.output[0])


if __name__ == '__main__':
    unittest.main()
