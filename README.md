# httpd-config-monitor

Using `systemd`'s `.path` unit to run a Python script whenever a `httpd` config file is changed. The Python script creates a backup of the config and logs the differences of the original file and the backup file. Then it calls the syntax checking command `httpd -t` and gracefully reloads the web server service with `httpd -k graceful`.
The script uses a YAML file for parameters like this:
```
---
httpd-conf: /path/to/file/you/want/to/watch
syntax-check-cmd: httpd -t
reload-cmd: httpd -k graceful
```
The `.path` file can be like this:

`httpd-monitor.path`:
```
[Unit]
Description=Monitor the httpd conf for changes

[Path]
PathChanged=/path/to/file
Unit=httpd-monitor.service

[Install]
WantedBy=multi-user.target
```
And the `.service` file like this:

`httpd-monitor.service`:
```
[Unit]
Description=Do backup, diff changes and reload httpd

[Service]
Restart=no
Type=simple
ExecStart=/path/to/httpd_monitor.py /path/to/httpd-monitor.yaml

[Install]
WantedBy=multi-user.target
```
So I'd first run `systemd start httpd-monitor.service` just to create the initial backup (the `if` condition within the `run()` method should take care of it) and then just `systemd enable --now httpd-monitor.path` to enable/start the `.path` unit.

Probably not the best and most scalable solution (using config management with say Ansible would be better no doubt) but as a DIY project to keep scripting skills somewhat sharp it fulfills its small purpose. 
